<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/register', [AuthController::class, 'daftar']);

Route::post('/welcome', [AuthController::class, 'sambutan']);

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-tables', function(){
    return view('page.data-tables');
});


//CRUD cast

//Create
//Form Tambah cast
Route::get('/cast/create', [castController::class, 'create']);

//untuk kirim data ke database atau tambah data ke database
Route::post('/cast', [castController::class, 'store']);

//Read
//Tampilkan semua
Route::get('/cast',[castController::class, 'index']);

//Detail cast bersdasarkan id
Route::get('/cast/{cast_id}',[castController::class, 'show']);

//Update cast
//Form update cast
Route::get('/cast/{cast_id}/edit',[castController::class, 'edit']);

//update data ke database berdasarkan id
Route::put('/cast/{cast_id}',[castController::class, 'update']);

//Delete
//Delete berdasarkan id
Route::delete('/cast/{cast_id}',[castController::class, 'destroy']);