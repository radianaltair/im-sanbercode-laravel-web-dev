@extends('layout.master')

@section('judul')
    <h1>Buat Account Baru!</h1>
@endsection

    
@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Others<br><br>
        <label>Nationality:</label><br><br>
        <select name="Nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Singapore">Singapore</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Australia">Australia</option>
        <option value="Others">Others</option>
    </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="languagespoken">Bahasa Indonesia<br>
        <input type="checkbox" name="languagespoken">English<br>
        <input type="checkbox" name="languagespoken">Others<br><br>
        <label>Bio:</label><br><br>
        <textarea rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value="Sign Up" href="/welcome">
    </form>
@endsection